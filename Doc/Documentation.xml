<?xml version="1.0"?>
<doc>
    <assembly>
        <name>FastCGI</name>
    </assembly>
    <members>
        <member name="T:FastCGI.Constants">
            <summary>
            FastCGI constants.
            Defined in the FastCGI specification section 8.
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_LISTENSOCK_FILENO">
            <summary>
            Listening socket file number
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_HEADER_LEN">
            <summary>
            Number of bytes in a FCGI_Header.
            Future versions of the protocol will not reduce this number.
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_VERSION_1">
            <summary>
            Value for version component of FCGI_Header
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_BEGIN_REQUEST">
            <summary>
            Values for type component of FCGI_Header
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_ABORT_REQUEST">
            <summary>
            Values for type component of FCGI_Header
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_END_REQUEST">
            <summary>
            Values for type component of FCGI_Header
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_PARAMS">
            <summary>
            Values for type component of FCGI_Header
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_STDIN">
            <summary>
            Values for type component of FCGI_Header
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_STDOUT">
            <summary>
            Values for type component of FCGI_Header
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_STDERR">
            <summary>
            Values for type component of FCGI_Header
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_DATA">
            <summary>
            Values for type component of FCGI_Header
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_GET_VALUES">
            <summary>
            Values for type component of FCGI_Header
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_GET_VALUES_RESULT">
            <summary>
            Values for type component of FCGI_Header
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_UNKNOWN_TYPE">
            <summary>
            Values for type component of FCGI_Header
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_MAXTYPE">
            <summary>
            Values for type component of FCGI_Header
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_NULL_REQUEST_ID">
            <summary>
            Value for requestId component of FCGI_Header
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_KEEP_CONN">
            <summary>
            Mask for flags component of FCGI_BeginRequestBody
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_RESPONDER">
            <summary>
            Values for role component of FCGI_BeginRequestBody
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_AUTHORIZER">
            <summary>
            Values for role component of FCGI_BeginRequestBody
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_FILTER">
            <summary>
            Values for role component of FCGI_BeginRequestBody
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_REQUEST_COMPLETE">
            <summary>
            Values for protocolStatus component of FCGI_EndRequestBody
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_CANT_MPX_CONN">
            <summary>
            Values for protocolStatus component of FCGI_EndRequestBody
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_OVERLOADED">
            <summary>
            Values for protocolStatus component of FCGI_EndRequestBody
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_UNKNOWN_ROLE">
            <summary>
            Values for protocolStatus component of FCGI_EndRequestBody
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_MAX_CONNS">
            <summary>
            Variable names for FCGI_GET_VALUES / FCGI_GET_VALUES_RESULT records
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_MAX_REQS">
            <summary>
            Variable names for FCGI_GET_VALUES / FCGI_GET_VALUES_RESULT records
            </summary>
        </member>
        <member name="F:FastCGI.Constants.FCGI_MPXS_CONNS">
            <summary>
            Variable names for FCGI_GET_VALUES / FCGI_GET_VALUES_RESULT records
            </summary>
        </member>
        <member name="T:FastCGI.FCGIApplication">
             <summary>
             Main FastCGI listener class.
             </summary>
             <remarks>
             This class manages a connection to a webserver by listening on a given port on localhost and receiving FastCGI requests by a webserver like Apache or nginx.
             Call <see cref="M:FastCGI.FCGIApplication.Run(System.Int32)"/> to start listening, and use <see cref="E:FastCGI.FCGIApplication.OnRequestReceived"/> to get notified of received requests.
             
             In FastCGI terms, this class implements the responder role. Refer to section 6.2 of the FastCGI specification for details.
             
             See the below example to learn how to accept requests. For more complex usage. have a look at the <see cref="T:FastCGI.Request"/> class.
             If you need to go even deeper, see the <see cref="T:FastCGI.Record"/> class and read the FastCGI specification: http://www.fastcgi.com/devkit/doc/fcgi-spec.html
             </remarks>
             <example>
             <code>
               // Create a new FCGIApplication, will accept FastCGI requests
               var app = new FCGIApplication();
            
               // Handle requests by responding with a 'Hello World' message
               app.OnRequestReceived += (sender, request) => {
                   request.WriteBodyASCII("Content-Type:text/html\n\nHello World!");
                   request.Close();
               };
               // Start listening on port 19000
               app.Run(19000);
             </code>
             </example>
        </member>
        <member name="F:FastCGI.FCGIApplication.OpenRequests">
            <summary>
            A dictionary of all open <see cref="T:FastCGI.Request">requests</see>, indexed by id.
            </summary>
        </member>
        <member name="F:FastCGI.FCGIApplication.CurrentStream">
            <summary>
            The network stream of the connection to the webserver.
            </summary>
            <remarks>
            Can be null if the application is currently not connected to a webserver.
            </remarks>
        </member>
        <member name="P:FastCGI.FCGIApplication.Connected">
            <summary>
            True iff this application is currently connected to a webserver.
            </summary>
        </member>
        <member name="F:FastCGI.FCGIApplication.Disconnecting">
            <summary>
            True iff this application is about to close the connection to the webserver.
            </summary>
        </member>
        <member name="E:FastCGI.FCGIApplication.OnRequestReceived">
            <summary>
            Will be called whenever a request has been received.
            </summary>
            <remarks>
            Please note that multiple requests can be open at the same time.
            This means that this event may fire multiple times before you call <see cref="M:FastCGI.Request.Close"/> on the first one.
            </remarks>
        </member>
        <member name="M:FastCGI.FCGIApplication.Run(System.Int32)">
            <summary>
            This method never returns! Starts listening for FastCGI requests on the given port.
            </summary>
        </member>
        <member name="M:FastCGI.FCGIApplication.SendRecord(FastCGI.Record)">
            <summary>
            Used internally. Sends the given record to the webserver.
            </summary>
        </member>
        <member name="T:FastCGI.Record">
            <summary>
            A FastCGI Record.
            </summary>
            <remarks>
            See section 3.3 of the FastCGI Specification for details.
            </remarks>
        </member>
        <member name="T:FastCGI.Record.RecordType">
            <summary>
            Record types, used in the 'type' field of Record.
            </summary>
            <remarks>
            Described in the FastCGI Specification section 8.
            </remarks>
        </member>
        <member name="T:FastCGI.Record.ProtocolStatus">
            <summary>
            Protocol status used for requests.
            Described in the FastCGI Specification section 8.
            </summary>
        </member>
        <member name="F:FastCGI.Record.Version">
            <summary>
            The version byte. Should always equal <see cref="F:FastCGI.Constants.FCGI_VERSION_1"/>.
            </summary>
        </member>
        <member name="F:FastCGI.Record.Type">
            <summary>
            The <see cref="T:FastCGI.Record.RecordType"/> of this record.
            </summary>
        </member>
        <member name="F:FastCGI.Record.RequestId">
            <summary>
            The request id associated with this record.
            </summary>
        </member>
        <member name="F:FastCGI.Record.ContentLength">
            <summary>
            The length of <see cref="F:FastCGI.Record.ContentData"/>.
            </summary>
        </member>
        <member name="F:FastCGI.Record.ContentData">
            <summary>
            The data contained in this record.
            </summary>
        </member>
        <member name="M:FastCGI.Record.GetNameValuePairs">
            <summary>
            Tries to read a dictionary of name-value pairs from the record content.
            </summary>
            <remarks>
            This method does not make any attempt to make sure whether this record actually contains a set of name-value pairs.
            It will return nonsense or throw an EndOfStreamException if the record content does not contain valid name-value pairs.
            </remarks>
        </member>
        <member name="M:FastCGI.Record.SetNameValuePairs(System.Collections.Generic.Dictionary{System.String,System.Byte[]})">
            <summary>
            Sets the record <see cref="F:FastCGI.Record.ContentData"/> to a given dictionary of name-value pairs.
            </summary>
        </member>
        <member name="M:FastCGI.Record.ReadVarLength(System.IO.Stream)">
            <summary>
            Reads a length from the given stream, which is encoded in one or four bytes.
            </summary>
            <remarks>
            See section 3.4 of the FastCGI specification for details.
            </remarks>
        </member>
        <member name="M:FastCGI.Record.ReadByte(System.IO.Stream)">
            <summary>
            Reads a single byte from the given stream.
            </summary>
        </member>
        <member name="M:FastCGI.Record.ReadInt16(System.IO.Stream)">
            <summary>
            Reads a 16-bit integer from the given stream.
            </summary>
        </member>
        <member name="M:FastCGI.Record.WriteInt16(System.IO.Stream,System.UInt16)">
            <summary>
            Writes a 16-bit integer to the given stream.
            </summary>
        </member>
        <member name="M:FastCGI.Record.WriteVarLength(System.IO.Stream,System.Int32)">
            <summary>
            Writes a length from the given stream, which is encoded in one or four bytes.
            </summary>
            <remarks>
            See section 3.4 of the FastCGI specification for details.
            </remarks>
        </member>
        <member name="M:FastCGI.Record.ReadRecord(System.IO.Stream)">
            <summary>
            Reads a single Record from the given stream.
            </summary>
            <returns>Returns the retreived record or null if no record could be read.</returns>
        </member>
        <member name="M:FastCGI.Record.WriteToStream(System.IO.Stream)">
            <summary>
            Writes this record to the given stream.
            </summary>
            <returns>Returns the number of bytes written.</returns>
        </member>
        <member name="M:FastCGI.Record.CreateStdout(System.Byte[],System.Int32)">
            <summary>
            Creates a Stdout record from the given data and request id
            </summary>
        </member>
        <member name="M:FastCGI.Record.CreateEndRequest(System.Int32)">
            <summary>
            Creates a EndRequest record with the given request id
            </summary>
        </member>
        <member name="M:FastCGI.Record.CreateGetValuesResult(System.Int32,System.Int32,System.Boolean)">
            <summary>
            Creates a GetValuesResult record from the given config values.
            </summary>
        </member>
        <member name="T:FastCGI.Request">
            <summary>
            A FastCGI request.
            </summary>
            <remarks>
            A request usually corresponds to a HTTP request that has been received by the webserver.
            You will probably want to use <see cref="M:FastCGI.Request.WriteResponse(System.Byte[])"/> or its helper methods to output a response and then call <see cref="M:FastCGI.Request.Close"/>.
            Use <see cref="E:FastCGI.FCGIApplication.OnRequestReceived"/> to be notified of new requests.
            Refer to the FastCGI specification for more details.
            </remarks>
        </member>
        <member name="P:FastCGI.Request.RequestId">
            <summary>
            The id for this request, issued by the webserver
            </summary>
        </member>
        <member name="F:FastCGI.Request.Parameters">
            <summary>
            The FastCGI parameters passed by the webserver.
            </summary>
            <remarks>
            All strings are encoded in ASCII, regardless of any encoding information in the request.
            </remarks>
        </member>
        <member name="P:FastCGI.Request.Body">
            <summary>
            The request body.
            </summary>
            <remarks>
            For POST requests, this will contain the POST variables. For GET requests, this will be empty.
            </remarks>
        </member>
        <member name="M:FastCGI.Request.HandleRecord(FastCGI.Record)">
            <summary>
            Used internally. Feeds a <see cref="T:FastCGI.Record">Record</see> to this request for processing.
            </summary>
            <param name="record">The record to feed.</param>
            <returns>Returns true iff the request is completely received.</returns>
        </member>
        <member name="M:FastCGI.Request.WriteResponse(System.Byte[])">
            <summary>
            Appends data to the response body.
            </summary>
            <remarks>
            The given data will be sent immediately to the webserver as a single stdout record.
            </remarks>
            <param name="data">The data to append.</param>
        </member>
        <member name="M:FastCGI.Request.WriteResponseASCII(System.String)">
            <summary>
            Appends an ASCII string to the response body.
            </summary>
            <remarks>
            This is a helper function, it converts the given string to ASCII bytes and feeds it to <see cref="M:FastCGI.Request.WriteResponse(System.Byte[])"/>.
            </remarks>
            <param name="data">The string to append, encoded in ASCII.</param>
            <seealso cref="M:FastCGI.Request.WriteResponse(System.Byte[])"/>
            <seealso cref="M:FastCGI.Request.WriteResponseUtf8(System.String)"/>
        </member>
        <member name="M:FastCGI.Request.WriteResponseUtf8(System.String)">
            <summary>
            Appends an UTF-8 string to the response body.
            </summary>
            <remarks>
            This is a helper function, it converts the given string to UTF-8 bytes and feeds it to <see cref="M:FastCGI.Request.WriteResponse(System.Byte[])"/>.
            </remarks>
            <param name="data">The string to append, encoded in UTF-8.</param>
            <seealso cref="M:FastCGI.Request.WriteResponse(System.Byte[])"/>
            <seealso cref="M:FastCGI.Request.WriteResponseASCII(System.String)"/>
        </member>
        <member name="M:FastCGI.Request.Close">
            <summary>
            Closes this request.
            </summary>
        </member>
    </members>
</doc>
