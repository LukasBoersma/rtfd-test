## Constants
*class FastCGI.Constants*

FastCGI constants.
            Defined in the FastCGI specification section 8.

**Static Fields**

* *Int32* **FCGI_LISTENSOCK_FILENO**

  Listening socket file number


* *Int32* **FCGI_HEADER_LEN**

  Number of bytes in a FCGI_Header.
            Future versions of the protocol will not reduce this number.


* *Int32* **FCGI_VERSION_1**

  Value for version component of FCGI_Header


* *Int32* **FCGI_BEGIN_REQUEST**

  Values for type component of FCGI_Header


* *Int32* **FCGI_ABORT_REQUEST**

  Values for type component of FCGI_Header


* *Int32* **FCGI_END_REQUEST**

  Values for type component of FCGI_Header


* *Int32* **FCGI_PARAMS**

  Values for type component of FCGI_Header


* *Int32* **FCGI_STDIN**

  Values for type component of FCGI_Header


* *Int32* **FCGI_STDOUT**

  Values for type component of FCGI_Header


* *Int32* **FCGI_STDERR**

  Values for type component of FCGI_Header


* *Int32* **FCGI_DATA**

  Values for type component of FCGI_Header


* *Int32* **FCGI_GET_VALUES**

  Values for type component of FCGI_Header


* *Int32* **FCGI_GET_VALUES_RESULT**

  Values for type component of FCGI_Header


* *Int32* **FCGI_UNKNOWN_TYPE**

  Values for type component of FCGI_Header


* *Int32* **FCGI_MAXTYPE**

  Values for type component of FCGI_Header


* *Int32* **FCGI_NULL_REQUEST_ID**

  Value for requestId component of FCGI_Header


* *Int32* **FCGI_KEEP_CONN**

  Mask for flags component of FCGI_BeginRequestBody


* *Int32* **FCGI_RESPONDER**

  Values for role component of FCGI_BeginRequestBody


* *Int32* **FCGI_AUTHORIZER**

  Values for role component of FCGI_BeginRequestBody


* *Int32* **FCGI_FILTER**

  Values for role component of FCGI_BeginRequestBody


* *Int32* **FCGI_REQUEST_COMPLETE**

  Values for protocolStatus component of FCGI_EndRequestBody


* *Int32* **FCGI_CANT_MPX_CONN**

  Values for protocolStatus component of FCGI_EndRequestBody


* *Int32* **FCGI_OVERLOADED**

  Values for protocolStatus component of FCGI_EndRequestBody


* *Int32* **FCGI_UNKNOWN_ROLE**

  Values for protocolStatus component of FCGI_EndRequestBody


* *String* **FCGI_MAX_CONNS**

  Variable names for FCGI_GET_VALUES / FCGI_GET_VALUES_RESULT records


* *String* **FCGI_MAX_REQS**

  Variable names for FCGI_GET_VALUES / FCGI_GET_VALUES_RESULT records


* *String* **FCGI_MPXS_CONNS**

  Variable names for FCGI_GET_VALUES / FCGI_GET_VALUES_RESULT records





---

## FCGIApplication
*class FastCGI.FCGIApplication*

Main FastCGI listener class.
This class manages a connection to a webserver by listening on a given port on localhost and receiving FastCGI requests by a webserver like Apache or nginx.
             Call  to start listening, and use  to get notified of received requests.
             
             In FastCGI terms, this class implements the responder role. Refer to section 6.2 of the FastCGI specification for details.
             
             See the below example to learn how to accept requests. For more complex usage. have a look at the  class.
             If you need to go even deeper, see the  class and read the FastCGI specification: http://www.fastcgi.com/devkit/doc/fcgi-spec.html

**Methods**

* *void* **[Run](#FastCGI.FCGIApplication.Run(System.Int32))** *(Int32 port)*

  This method never returns! Starts listening for FastCGI requests on the given port.


**Properties**

* *Boolean* **Connected**

  True iff this application is currently connected to a webserver.



**Fields**

* *Dictionary&lt;Int32, Request&gt;* **OpenRequests**

  A dictionary of all open requests, indexed by id.


* *Stream* **CurrentStream**

  The network stream of the connection to the webserver.


* *Boolean* **Disconnecting**

  True iff this application is about to close the connection to the webserver.




<a id="FastCGI.FCGIApplication.Run(System.Int32)"></a>
### void Run(Int32 port)
This method never returns! Starts listening for FastCGI requests on the given port.


**Parameters:**
* *Int32* **port**



---

## Record
*class FastCGI.Record*

A FastCGI Record.
See section 3.3 of the FastCGI Specification for details.

**Methods**

* *Dictionary&lt;String, Byte[]&gt;* **[GetNameValuePairs](#FastCGI.Record.GetNameValuePairs)** *()*

  Tries to read a dictionary of name-value pairs from the record content.

* *void* **[SetNameValuePairs](#FastCGI.Record.SetNameValuePairs(System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Byte[], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]))** *(Dictionary&lt;String, Byte[]&gt; nameValuePairs)*

* *Int32* **[WriteToStream](#FastCGI.Record.WriteToStream(System.IO.Stream))** *(Stream stream)*

  Writes this record to the given stream.

* *String* **[ToString](#FastCGI.Record.ToString)** *()*


**Fields**

* *Byte* **Version**

  The version byte. Should always equal .


* *RecordType* **Type**

  The  of this record.


* *Int32* **RequestId**

  The request id associated with this record.


* *Int32* **ContentLength**

  The length of .


* *Byte[]* **ContentData**

  The data contained in this record.



**Methods**

* *Record* **[ReadRecord](#FastCGI.Record.ReadRecord(System.IO.Stream))** *(Stream stream)*

  Reads a single Record from the given stream.

* *Record* **[CreateStdout](#FastCGI.Record.CreateStdout(System.Byte[],System.Int32))** *(Byte[] data, Int32 requestId)*

  Creates a Stdout record from the given data and request id

* *Record* **[CreateEndRequest](#FastCGI.Record.CreateEndRequest(System.Int32))** *(Int32 requestId)*

  Creates a EndRequest record with the given request id

* *Record* **[CreateGetValuesResult](#FastCGI.Record.CreateGetValuesResult(System.Int32,System.Int32,System.Boolean))** *(Int32 maxConnections, Int32 maxRequests, Boolean multiplexing)*

  Creates a GetValuesResult record from the given config values.



<a id="FastCGI.Record.GetNameValuePairs"></a>
### Dictionary&lt;String, Byte[]&gt; GetNameValuePairs()
Tries to read a dictionary of name-value pairs from the record content.
This method does not make any attempt to make sure whether this record actually contains a set of name-value pairs.
            It will return nonsense or throw an EndOfStreamException if the record content does not contain valid name-value pairs.



<a id="FastCGI.Record.WriteToStream(System.IO.Stream)"></a>
### Int32 WriteToStream(Stream stream)
Writes this record to the given stream.

**Returns:** Returns the number of bytes written.

**Parameters:**
* *Stream* **stream**


<a id="FastCGI.Record.ReadRecord(System.IO.Stream)"></a>
### Record ReadRecord(Stream stream)
Reads a single Record from the given stream.

**Returns:** Returns the retreived record or null if no record could be read.

**Parameters:**
* *Stream* **stream**


<a id="FastCGI.Record.CreateStdout(System.Byte[],System.Int32)"></a>
### Record CreateStdout(Byte[] data, Int32 requestId)
Creates a Stdout record from the given data and request id


**Parameters:**
* *Byte[]* **data**

* *Int32* **requestId**


<a id="FastCGI.Record.CreateEndRequest(System.Int32)"></a>
### Record CreateEndRequest(Int32 requestId)
Creates a EndRequest record with the given request id


**Parameters:**
* *Int32* **requestId**


<a id="FastCGI.Record.CreateGetValuesResult(System.Int32,System.Int32,System.Boolean)"></a>
### Record CreateGetValuesResult(Int32 maxConnections, Int32 maxRequests, Boolean multiplexing)
Creates a GetValuesResult record from the given config values.


**Parameters:**
* *Int32* **maxConnections**

* *Int32* **maxRequests**

* *Boolean* **multiplexing**



---

## Request
*class FastCGI.Request*

A FastCGI request.
A request usually corresponds to a HTTP request that has been received by the webserver.
            You will probably want to use  or its helper methods to output a response and then call .
            Use  to be notified of new requests.
            Refer to the FastCGI specification for more details.

**Methods**

* *void* **[WriteResponse](#FastCGI.Request.WriteResponse(System.Byte[]))** *(Byte[] data)*

  Appends data to the response body.

* *void* **[WriteResponseASCII](#FastCGI.Request.WriteResponseASCII(System.String))** *(String data)*

  Appends an ASCII string to the response body.

* *void* **[WriteResponseUtf8](#FastCGI.Request.WriteResponseUtf8(System.String))** *(String data)*

  Appends an UTF-8 string to the response body.

* *void* **[Close](#FastCGI.Request.Close)** *()*

  Closes this request.


**Properties**

* *Int32* **RequestId**

  The id for this request, issued by the webserver


* *String* **Body**

  The request body.



**Fields**

* *Dictionary&lt;String, String&gt;* **Parameters**

  The FastCGI parameters passed by the webserver.




<a id="FastCGI.Request.WriteResponse(System.Byte[])"></a>
### void WriteResponse(Byte[] data)
Appends data to the response body.
The given data will be sent immediately to the webserver as a single stdout record.


**Parameters:**
* *Byte[]* **data**

  The data to append.


<a id="FastCGI.Request.WriteResponseASCII(System.String)"></a>
### void WriteResponseASCII(String data)
Appends an ASCII string to the response body.
This is a helper function, it converts the given string to ASCII bytes and feeds it to .


**Parameters:**
* *String* **data**

  The string to append, encoded in ASCII.


<a id="FastCGI.Request.WriteResponseUtf8(System.String)"></a>
### void WriteResponseUtf8(String data)
Appends an UTF-8 string to the response body.
This is a helper function, it converts the given string to UTF-8 bytes and feeds it to .


**Parameters:**
* *String* **data**

  The string to append, encoded in UTF-8.


<a id="FastCGI.Request.Close"></a>
### void Close()
Closes this request.




---

